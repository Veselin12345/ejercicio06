/*
  Ejercicio: Interfaces RESTs
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: Programa que utiliza el API de Open Weather Map y presenta en una etiqueta HTML específica
*/
// Inicializamos módulos a utilizar
var http = require('http');
var path = require('path');
var jade = require('jade');
var restler = require('restler');
var IP = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
// Creamos servidor HTTP
http.createServer(function(peticion, respuesta){
	// Obtenemos nombre de archivo solicitado
	var nombreArchivo = '.' + peticion.url;
	console.log('Archivo solicitado: ' + nombreArchivo); 
	if(nombreArchivo == './' || nombreArchivo == './index.html')
		nombreArchivo = './index.jade';
	// Obtenemos extensión de archivo solicitado
	var extensionArchivo = path.extname(nombreArchivo);
	var contentType = 'text/html';
	// Obtenemos datos de API REST
	if(nombreArchivo == './index.jade'){
		restler.get('http://api.openweathermap.org/data/2.5/weather?q=London&appid=2de143494c0b295cca9337e1e96b00e0&lang=es&units=metric').on('complete', function(datos){
			jade.renderFile(nombreArchivo, {datos: datos}, function(error, archivo){
				respuesta.writeHead(200, {'Content-Type': contentType});
				respuesta.end(archivo, 'utf-8');
			});
		});
	}
	else{
		jade.renderFile('./404.jade', function(error, archivo){
			respuesta.writeHead(200, {'Content-Type': contentType});
			respuesta.end(archivo, 'utf-8');
		});
	}
}).listen(PORT, IP);
console.log('Servidor ejecutándose en: http://' + IP + ':' + PORT + '/');